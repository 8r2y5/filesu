#!/usr/bin/env python
#-*- coding: utf-8 -*-

import select
import os
import socket
import threading
import time
import sys
from hashlib import md5
import types
import logging


class FilesUClient(threading.Thread):
    '''
    Klasa klienta do przesylu danych, i plikow i stringow.
    '''

    def __init__(self, c_socket, c_address, id_number, list_of_files_string, db_of_files,
                 salvation):
        '''
        Konstruktor klasy, przyjmuje, obiekt polaczenia, adres polaczenia oraz liste
        plikow od glownego watku
        '''
        threading.Thread.__init__(self)
        self.__logger = logging.getLogger('FilesU.Client-%s-%s' % (id_number,
                                                                   '%s:%s' % c_address))
        #self.__logger.setLevel(logging.DEBUG)
        self.__logger.info('Initializing')
        self.__socket = c_socket
        self.__socket.settimeout(5)
        self.__address = c_address
        self.__list_of_files_string = list_of_files_string
        self.__db_of_files = db_of_files
        self.__id_number = id_number
        self.__salvation = salvation

        self.__running = 1
        self.__logger.info('Initialization ended successful')


    def stop(self):
        self.__logger.info('Parent is stopping me')
        self.__running = 0


    def __send_a_file(self, f, maxsize=1024 * 4):
        '''
        Prywatna metoda przesylania plikow w iteracji (pomocne w przesylaniu duzych
        plikow nie wczytujac calych do pamieci).
        '''
        for x in iter((lambda: f.read(maxsize)), ''):
            self.send_msg(x)

        time.sleep(0.1)
        #while self.__running:
            #    buffer = file.read(maxsize)
            #    if buffer:
            #        self.send_msg(buffer)
            #    else:
            #        break


    def __send_a_string(self, long_string, maxsize=1024 * 4):
        '''
        Prywatna metoda przesylania ciagow znakow.
        '''
        time.sleep(1)
        gen = (long_string[x:x + maxsize] for x in xrange(0, len(long_string), maxsize))
        for x in gen:
            self.send_msg(x)


    def send_m0ar(self, something):
        '''
        Metoda wybiera ktora metoda wyslac dane
        '''
        if types.FileType == type(something):
            self.__logger.debug('Sending as file')
            self.__send_a_file(something)

        elif types.StringType == type(something):
            self.__logger.debug('Sending as string')
            self.__send_a_string(something)


    def menu(self, msg):
        '''
        Metoda "menu"
        '''
        if msg:
            if msg == 'daj mi liste':
                self.__logger.info('Sending list of files to client')

                self.send_m0ar(self.__list_of_files_string)

                self.__logger.info('Sending list of files to client completed')

            elif msg.find('wyslij mi:') == 0:
                that_file = msg.lstrip('wyslij mi:')
                self.__logger.info('Client is asking for %s' % that_file)

                if self.__db_of_files.has_key(that_file):
                    self.send_msg(
                        'Ok, i will send you %s bytes' % self.__db_of_files[that_file][1])
                    odp = self.get_msg()
                    if odp:
                        if odp == 'yes':
                            self.__logger.info('Sending file %s to client' % that_file)

                            full_path_to_file = self.__db_of_files[that_file][0]
                            with open(full_path_to_file) as f:
                                self.__send_a_file(f, 1024 * 4)

                            self.__logger.info(
                                'Sending file %s to client completed' % that_file)
                        else:
                            self.__logger.warning(
                                'Answer of sending was not yes: "%s"' % odp)

                    else:
                        self.__logger.warning(
                            'On requesting answer, I got %s' % type(odp))

                else:
                    self.__logger.warning(
                        'There is no such file like %s in list' % that_file)
                    self.send_msg('There is no such file')

            elif msg == 'bye bye':
                self.__logger.info('Disconnecting')
                self.__running = 0
        else:
            self.__logger.warning('Got data that is %s' % type(msg))
            self.__running = 0


    def send_msg(self, msg):
        '''
        Metoda wysylania danych, gdy wystapi problem wylacza glowna petle.
        '''
        try:
            self.__socket.send(msg)
        except:
            self.__logger.debug('Oh my gosh, an exception on sending!', exc_info=True)
            self.__running = 0


    def get_msg(self, maxsize=1024):
        '''
        Metoda odbierania danych, argument nie jest potrzebny.
        '''
        try:
            msg = self.__socket.recv(maxsize)
        except:
            self.__logger.critical('Oh my gosh, an exception on reciving!', exc_info=True)
            self.__running = 0
            return None
        else:
            return msg


    def run(self):
        '''
        Glowna metoda klasy, przekazuje wiadomosc do menu
        '''
        max_idle = 10
        idle = 0
        while self.__running:
            i_rdy = select.select([self.__socket], [], [], 1)[0]

            if self.__socket in i_rdy:
                self.menu(self.get_msg())
                idle = 0

            else:
                idle += 1
                #print idle

            if idle >= max_idle:
                self.__logger.info("Stopping because connection is idling")
                self.__running = 0

        self.__logger.info('iStop')
        self.__salvation(self.__id_number)


class FilesUServer:
    '''
    Glowna klasa odpowiadajaca za przekazywanie nowych polaczen
    '''

    def __init__(self, directory='.', bind_to_ip='localhost', bind_to_port=27798):
        self.__logger = logging.getLogger('FilesU.Server')
        self.__logger.info('Initialazing Server')
        self.__full_directory_path = directory.rstrip('/')
        self.__short_directory_path = os.path.split(self.__full_directory_path)[1]

        self.__bind_to_adres = (bind_to_ip, bind_to_port)

        if not os.path.exists(directory):
            self.__logger.error('!!! %s does not exists !!!' % directory)
            sys.exit(1)

        self.__logger.info('Starting indexing files')
        self.__indexed_files = self.index_files(directory)
        self.__indexed_files_for_json = ''

        for sort_path, file_info in self.__indexed_files.iteritems():
            #self.__indexed_files_for_json[sort_path] = {
            #    'size in bytes': file_info[1],
            #    'md5': file_info[2]
            #}
            self.__indexed_files_for_json += sort_path + ';' + ';'.join(
                file_info[1:]) + '\n'

        self.__logger.info('Ended indexing files')

        self.__logger.info('Starting preparing socket')
        self.__socket = self.prepare_socket(self.__bind_to_adres)
        self.__logger.info('Ended preparing socket successful')
        self.__logger.info('Intialization ended successful')


    def salvation(self, id_number):
        self.__logger.info('Trying to delete thread nr %s...' % id_number)
        if self.__clients_threads.has_key(id_number):
            del self.__clients_threads[id_number]
            self.__logger.info('Thread nr %s destroyed' % id_number)
        else:
            self.__logger.info('There is no thread by nr %s' % id_number)


    def index_files(self, directory):
        tmp = {}
        for r, d, files in os.walk(directory):
            for file in files:
                sort_path = os.path.join(r, file).lstrip(
                    os.path.split(self.__full_directory_path)[0])
                self.__logger.info(
                    'Indexing: %s' % sort_path)

                full_file_path = os.path.join(r, file)
                self.__logger.info(' - %s' % full_file_path)

                file_size = os.path.getsize(full_file_path)
                self.__logger.info(
                    ' - %.2f mb <> %s' % (file_size / (1024.0 ** 2), str(file_size)))

                file_md5 = md5()
                with open(full_file_path, 'rb') as f:
                    for seg in iter((lambda: f.read(1024)), ''):
                        file_md5.update(seg)

                file_md5 = file_md5.hexdigest()
                self.__logger.info(' - %s\n' % file_md5)

                tmp[sort_path] = (full_file_path, str(file_size), file_md5)

        return tmp

    def prepare_socket(self, bind_to_adres):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        prepare = 1

        while prepare:
            try:
                s.bind(bind_to_adres)
            except Exception, e:
                self.__logger.error('Oh noes! I got an error: %s' % e, exc_info=True)
                self.__logger.error('Waiting 15 sec')
                time.sleep(15)
            else:
                prepare = 0

        return s


    def stop(self):
        self.__running = 0
        self.__clients_threads = []
        try:
            self.__socket.close()
        except Exception, e:
            print e


    def run(self):
        self.__socket.listen(5)
        self.__clients_threads = {}
        self.__running = 1
        self.__logger.info('Waiting for connections')
        id_number = 0
        while self.__running:
            i_rdy = select.select([self.__socket, sys.stdin], [], [], 1)[0]

            if self.__socket in i_rdy:
                client_socket, client_address = self.__socket.accept()
                self.__logger.info('New connection from %s:%s' % client_address)

                self.__logger.debug('Initializing object for connection')
                client_thread = FilesUClient(client_socket, client_address, id_number,
                                             self.__indexed_files_for_json,
                                             self.__indexed_files, self.salvation)
                self.__logger.debug('Initialization object ended successful')

                self.__logger.debug('Adding reference to array')
                self.__clients_threads[id_number] = (client_socket,
                                                     client_address,
                                                     client_thread)

                self.__logger.debug('Setting thread to daemon')
                client_thread.Daemon = True

                self.__logger.debug('Starting reference')
                client_thread.start()

                id_number += 1

            if sys.stdin in i_rdy:
                junk = sys.stdin.readline()
                if junk.strip() == 'exit' or junk.strip() == 'stop':
                    self.__logger.info('Stopping server...')
                    self.__running = 0

        self.__logger.info('Server stopped listening for connections')
        self.__logger.info('Stopping active connection')
        for number, data in self.__clients_threads.items():
            if data[2].isAlive:
                self.__logger.info('Stopping thread nr %s - ip %s' % (number,
                                                                      '%s:%s' % data[1]))
                data[2].stop()
                data[2].join(10)
        else:
            self.__logger.info('Threads stopped')
        try:
            self.__socket.close()
        except:
            pass

        del self.__clients_threads

what_log = logging.DEBUG
data_format = '%d-%m-%Y %H:%M:%S'
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt=data_format)

formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                              datefmt=data_format)

log = logging.getLogger('FilesU')
log.setLevel(what_log)

#log_stream = logging.StreamHandler()
#log_stream.setLevel(what_log)

log_file = logging.FileHandler('FilesUServer.log')
log_file.setLevel(what_log)

#log_stream.setFormatter(formatter)
log_file.setFormatter(formatter)

#log.addHandler(log_stream)
log.addHandler(log_file)

#logging.info('lolz')
if __name__ == '__main__':
    fus = FilesUServer('/home/brzys/Steam/common/Uplink/', '192.168.1.6')
    fus.run()
